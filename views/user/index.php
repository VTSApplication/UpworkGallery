<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
			[
				'label' => 'Gallery Type',
				'value' => function ($data) {
                return $data->gallery; // $data['name'] for array data, e.g. using SqlDataProvider.
            },
			],
			
			[
				'label' => 'Gallery Images',
				'format' => 'raw',
				'value' => function ($data) {
				$get_images = $data->getGalleryImages($data->galleryid);
				$all_images = explode(',',$get_images);
				$image_count = count($all_images);
				if($image_count > 0){
					
					$images = '';
					for($i=0; $i<$image_count; $i++){
						$images .= '<img class="up-gallery-image" src="'.Yii::getAlias('@web').'/uploads/'.$all_images[$i].'" />';
					}
                return $images; // $data['name'] for array data, e.g. using SqlDataProvider.
				}
				else
				return 'No Images Found';
            },
			],			

            ['class' => 'yii\grid\ActionColumn',
			'header'=>'Edit Gallery',
			'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
