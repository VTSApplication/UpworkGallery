<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'gallery_type')->dropDownList(['art'=>'Art','fiction'=>'Fiction','scary'=>'Scary','natural'=>'Natural'],['prompt'=>'Select Gallery Type']); ?>	
	<?= $form->field($model, 'gallery_image1')->fileInput() ?>
	<?= Html::img('@web/images/empty.png', ['class' => 'image-previews','id'=>'user-gallery_image1_preview']) ?>
	<?= $form->field($model, 'gallery_image2')->fileInput() ?>
	<?= Html::img('@web/images/empty.png', ['class' => 'image-previews','id'=>'user-gallery_image2_preview']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
