<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//For Getting Gallery Type
use app\models\Gallerytype;

//For Save/Update/Delete Gallery Image
use app\models\Galleryimage;

//For Mapping Array
use yii\helpers\ArrayHelper;

//For Upload File
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
    $query = User::find()
        ->select(['user.*', 'gallerytype.*'])
        ->joinWith('gallerytype')
		->where(['<>','user.id', 1]);	

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);		

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

			if ($model->load(Yii::$app->request->post())) {
				
				
				//For Upload Images
				$model->gallery_image1 = UploadedFile::getInstance($model, 'gallery_image1');
				$model->gallery_image2 = UploadedFile::getInstance($model, 'gallery_image2');
				if ($model->gallery_image1 && $model->gallery_image2 && $model->validate()) { 
					$model->save();
					$user_id = $model->id;
					$model->gallery_image1->saveAs('uploads/' . $model->gallery_image1->baseName.$user_id . '.' . $model->gallery_image1->extension);
					$model->gallery_image2->saveAs('uploads/' . $model->gallery_image2->baseName.$user_id . '.' . $model->gallery_image2->extension);
					
					
					//For Save Details in Gallery Table
					
					$model_gallery = new Gallerytype();
					
					$model_gallery->userid = $model->id;
					
					$model_gallery->gallery = $model->gallery_type;
					
					$model_gallery->save();
					
					print_r($model_gallery->getErrors());
									
					
				//For Save Details in Images Table
				
				for($i=1; $i<=2; $i++){
					
				$model_images = new Galleryimage();
				
				$images = "gallery_image".$i;
				
				$image_link = $model->$images->baseName.$user_id . '.' . $model->$images->extension;

				$model_images->galleryid = $model_gallery->galleryid;
				
				$model_images->imagelink = $image_link;
				
				$model_images->save();
				
				print_r($model_images->getErrors());
					
				}			
				
				return $this->redirect(['index']);
			}
		}

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$model->scenario = 'update_gallery';
		
		$get_gallerytype = Gallerytype::find()
						   ->select(['gallerytype.*,GROUP_CONCAT(galleryimage.imagelink) AS imagelinks,GROUP_CONCAT(galleryimage.imageid) AS imageids'])
						   ->joinWith('galleryimage')
						   ->where(['=','userid',$model->id])->one();
						   
		$galleryid = $get_gallerytype->galleryid;
		
		$get_images = $get_gallerytype->imagelinks;
		
		$get_image_ids = $get_gallerytype->imageids;
		
		$all_images = explode(',',$get_images);	

		$all_image_ids = explode(',',$get_image_ids);			

        if ($model->load(Yii::$app->request->post())) {
			
				//For Upload Images
				$model->gallery_image1 = UploadedFile::getInstance($model, 'gallery_image1');
				
				$model->gallery_image2 = UploadedFile::getInstance($model, 'gallery_image2');
				
				$model->save();
				
				
				//For Update Gallery Type
				$update_gallery_type = Gallerytype::find()->where(['=','galleryid',$galleryid])->one();
				
				$update_gallery_type->gallery = $model->gallery_type;
				
				$update_gallery_type->save();
				
				for($i=1; $i<=2; $i++){
				$images = "gallery_image".$i;
				
				
					if ($model->$images && $model->validate()) {

						$prev_image = $all_images[($i-1)];
						
						$image_id = $all_image_ids[($i-1)];
						
						if (file_exists('uploads/'.$prev_image)) {							
							unlink('uploads/'.$prev_image);						
						}						
						
						//For Update User						
						
						$model->$images->saveAs('uploads/' . $model->$images->baseName . '.' . $model->$images->extension);	

						$update_images = Galleryimage::find()->where(['=','imageid',$image_id])->one();					
						
						//For Update Details in Images Table												
						
						$image_link = $model->$images->baseName . '.' . $model->$images->extension;

						$update_images->galleryid = $galleryid;
						
						$update_images->imagelink = $image_link;
						
						$update_images->save();
						
						echo "Updated";
						
					}	
				}
            return $this->redirect(['index']);														            
        }
		
		
		$gallerytype = $get_gallerytype->gallery;
		
		$model->gallery_type = $gallerytype;
		
		$image_count = count($all_images);
		if($image_count > 0){
			
		$images = array();
		
			for($i=0; $i<$image_count; $i++){
				$images[] = '<div class="update-image-div"><img class="update-gallery-image" src="'.Yii::getAlias('@web').'/uploads/'.$all_images[$i].'" /></div>';
			}		
		}

        return $this->render('update', [
            'model' => $model,'images'=>$images
        ]);
    }
	

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		//Find Gallery to delete
		$find_gallery_user = Gallerytype::find()->where(['=','userid',$id])->one();
		
		//Find Gallery images to delete
		if($find_gallery_user){
			
			$galleryid = $find_gallery_user->galleryid;
			
			$find_gallery_user_images = Galleryimage::find()->where(['=','galleryid',$galleryid])->all();
			
			foreach ($find_gallery_user_images as $img){
				
				unlink('uploads/'.$img->imagelink);
			}
			
			Galleryimage::deleteAll(['=','galleryid',$galleryid]);
			
			Gallerytype::deleteAll(['=','userid',$id]);
		}
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
