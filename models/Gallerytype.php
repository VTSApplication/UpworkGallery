<?php

namespace app\models;

use Yii;

use app\models\Galleryimage;

/**
 * This is the model class for table "gallerytype".
 *
 * @property int $id
 * @property string $gallery
 */
class Gallerytype extends \yii\db\ActiveRecord
{
	public $imagelinks,$imageids;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gallerytype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallery','userid'], 'required'],
            [['gallery'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gallery' => 'Gallery',
        ];
    }
	
	//Get Gallery Type
	public function getGalleryimage(){
		return $this->hasMany(Galleryimage::className(), ['galleryid' => 'galleryid']);
	}	
}
