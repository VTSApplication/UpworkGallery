<?php

namespace app\models;

use yii\db\ActiveRecord;

use app\models\Gallerytype;

use app\models\Galleryimage;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

public $gallery_type,$gallery,$imagelinks,$gallery_image1,$gallery_image2,$galleryid;

public static function tableName() { 

	return 'user'; 

}


public function rules()
{
	return [
		[['username','gallery_type','gallery_image1','gallery_image2'], 'required'],
		['gallery_image1', 'file', 'extensions' => ['png', 'gif', 'jpg']],
		['gallery_image2', 'file', 'extensions' => ['png', 'gif', 'jpg']]
	];
}



/**
* @Scenarios
*/
public function scenarios() {
	$scenarios = parent::scenarios();
	$scenarios['update_gallery'] = ['username','gallery_type'];//Scenario Values Only Accepted
return $scenarios;
}

   /**
 * @inheritdoc
 */
public static function findIdentity($id) {
    $user = self::find()
            ->where([
                "id" => $id
            ])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}

/**
 * @inheritdoc
 */
public static function findIdentityByAccessToken($token, $userType = null) {

    $user = self::find()
            ->where(["accessToken" => $token])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}

/**
 * Finds user by username
 *
 * @param  string      $username
 * @return static|null
 */
public static function findByUsername($username) {
    $user = self::find()
            ->where([
                "username" => $username
            ])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}

/**
 * @inheritdoc
 */
public function getId() {
    return $this->id;
}

/**
 * @inheritdoc
 */
public function getAuthKey() {
    return $this->auth_key;
}

/**
 * @inheritdoc
 */
public function validateAuthKey($authKey) {
    return $this->auth_key === $authKey;
}

/**
 * Validates password
 *
 * @param  string  $password password to validate
 * @return boolean if password provided is valid for current user
 */
public function validatePassword($password) {
    return $this->password === $password;
}

//Get Gallery Type
public function getGallerytype(){
    return $this->hasMany(Gallerytype::className(), ['userid' => 'id']);
}

//Get Gallery Type
public function getGalleryImages($data){

     $query_images = GalleryImage::find()
	    ->select(['GROUP_CONCAT(imagelink) AS imagelinks'])
		->groupBy(['galleryid'])
		->where(['=','galleryid',$data])->all();
		
	return $query_images[0]->imagelinks;
}

}

?>