<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "galleryimage".
 *
 * @property int $id
 * @property int $galleryid
 * @property int $userid
 * @property string $imagelink
 * @property string $created
 */
class Galleryimage extends \yii\db\ActiveRecord
{
	
	public $imagelinks;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galleryimage';
    }
	
	
    /**
     * {@inheritdoc}
     */	
	public static function primaryKey()
	{
		return ['imageid'];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['galleryid', 'imagelink'], 'required'],
            [['galleryid'], 'integer'],
            [['imagelink'], 'string'],
            [['created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'galleryid' => 'Galleryid',
            'userid' => 'Userid',
            'imagelink' => 'Imagelink',
            'created' => 'Created',
        ];
    }
}
