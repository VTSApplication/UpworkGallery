function readURL(input,img_id) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
	 $('#'+img_id+'_preview').show();
	 $('#'+img_id+'_preview').next('.update-image-div').hide();
      $('#'+img_id+'_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(document).ready(function(){
	$('input[type="file"]').change(function() {
	  readURL(this,$(this).attr('id'));
	});	
});